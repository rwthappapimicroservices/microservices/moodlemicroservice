using System.Collections.Generic;

namespace PIT.Labs.RWTHApp.DataSources.Moodle
{

    public class SiteInfoFunctionExternal
    {
        public string name { get; set; }    // function name
        public string version { get; set; } // The version number of the component to which the function belongs

        public SiteInfoFunctionExternal()
        { }

        public SiteInfoFunctionExternal(SiteInfoFunctionExternal copy)
        {
            name = copy.name;
            version = copy.version;
        }
    }

    public class SiteInfoAdvancedFeatureExternal
    {
        public string name { get; set; } // feature name
        public int value { get; set; }   // feature value. Usually 1 means enabled.

        public SiteInfoAdvancedFeatureExternal()
        { }

        public SiteInfoAdvancedFeatureExternal(SiteInfoAdvancedFeatureExternal copy)
        {
            name = copy.name;
            value = copy.value;
        }
    }

    public class SiteInfoExternal: MoodleResponse
    {
        public string sitename { get; set; }        // site name
        public string username { get; set; }        // username
        public string firstname { get; set; }       // first name
        public string lastname { get; set; }        // last name
        public string fullname { get; set; }        // user full name
        public string lang { get; set; }            // Current language.
        public int userid { get; set; }             // user id
        public string siteurl { get; set; }         // site url
        public string userpictureurl { get; set; }  // the user profile picture.
        // Warning: this url is the public URL that only works when forcelogin is set to NO and guestaccess is set to YES.
        // In order to retrieve user profile pictures independently of the Moodle config, replace "pluginfile.php" by
        //             "webservice/pluginfile.php?token=WSTOKEN&file="
        //             Of course the user can only see profile picture depending
        //             on his/her permissions. Moreover it is recommended to use HTTPS too.
        public List<SiteInfoFunctionExternal> functions { get; set; } // functions that are available
        public int? downloadfiles { get; set; }          // (optional) 1 if users are allowed to download files, 0 if not
        public int? uploadfiles { get; set; }            // (optional) 1 if users are allowed to upload files, 0 if not
        public string release { get; set; }              // (optional) Moodle release number
        public string version { get; set; }              // (optional) Moodle version number
        public string mobilecssurl { get; set; }         // (optional) Mobile custom CSS theme
        public List<SiteInfoAdvancedFeatureExternal> advancedfeatures { get; set; }  // (optional) Advanced features availability
        public bool? usercanmanageownfiles { get; set; }  // (optional) true if the user can manage his own files
        public int? userquota { get; set; }              // (optional) user quota (bytes). 0 means user can ignore the quota
        public int? usermaxuploadfilesize { get; set; }  // (optional) user max upload file size (bytes). -1 means the user can ignore the upload file size
        public int? userhomepage { get; set; }           // (optional) the default home page for the user: 0 for the site home, 1 for dashboard
        public int? siteid { get; set; }                 // (optional) Site course ID
        public string sitecalendartype { get; set; }     // (optional) Calendar type set in the site.
        public string usercalendartype { get; set; }     // (optional) Calendar typed used by the user.

        public SiteInfoExternal()
        { }

        public SiteInfoExternal(SiteInfoExternal copy): base(copy)
        {
            sitename = copy.sitename;
            username = copy.username;
            firstname = copy.firstname;
            lastname = copy.lastname;
            fullname = copy.fullname;
            lang = copy.lang;
            userid = copy.userid;
            siteurl = copy.siteurl;
            userpictureurl = copy.userpictureurl;
            downloadfiles = copy.downloadfiles;
            uploadfiles = copy.uploadfiles;
            release = copy.release;
            version = copy.version;
            mobilecssurl = copy.mobilecssurl;
            usercanmanageownfiles = copy.usercanmanageownfiles;
            userquota = copy.userquota;
            usermaxuploadfilesize = copy.usermaxuploadfilesize;
            userhomepage = copy.userhomepage;
            siteid = copy.siteid;
            sitecalendartype = copy.sitecalendartype;
            usercalendartype = copy.usercalendartype;
            functions = new List<SiteInfoFunctionExternal>();
            copy.functions.ForEach((item) =>
                {
                    functions.Add(new SiteInfoFunctionExternal(item));
                }
            );
            advancedfeatures = new List<SiteInfoAdvancedFeatureExternal>();
            copy.advancedfeatures.ForEach((item) =>
                {
                    advancedfeatures.Add(new SiteInfoAdvancedFeatureExternal(item));
                }
            );
        }
    }
}
