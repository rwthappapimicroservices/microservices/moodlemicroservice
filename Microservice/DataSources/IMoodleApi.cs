using System.Collections.Generic;
using System.IO;

namespace PIT.Labs.RWTHApp.DataSources.Moodle
{
    public interface IMoodleApi
    {
        string extTokenLogin(string oauth_token);
        SiteInfoExternal GetSiteInfo(string moodle_token);
        SiteInfoExternal GetSiteInfoAdmin();
        MoodleResponseList<CategoryExternal> GetCourseCategories();
        MoodleResponseList<CourseExternal> GetCourses(string moodle_token);
        MoodleResponseList<EnrolCourseExternal> GetEnrolledCourses(int user_id, string moodle_token);
        MoodleResponseList<EnrolCourseExternal> GetMyEnrolledCourses(string moodle_token);
        MoodleResponseList<CourseExternal> GetCoursesByIds(List<int> courseids, string moodle_token);
        MoodleResponseList<CourseContentExternal> GetCourseContents(int courseid, string moodle_token);
        MoodleResponseList<CourseUserProfileExternal> GetMyRolesById(int userid, int courseid, string moodle_token);
        Stream DownloadFile(string filename, string downloadurl, string moodle_token);
        string GetSemester(int semester_offset);
    }
}
