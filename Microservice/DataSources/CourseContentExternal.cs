using System.Collections.Generic;

namespace PIT.Labs.RWTHApp.DataSources.Moodle
{
    public class CourseContentModuleContentExternal
    {
        public string type { get; set; } //a file or a folder or external link
        public string filename { get; set; } //filename
        public string filepath { get; set; } //filepath
        public int? filesize { get; set; } //filesize
        public string fileurl { get; set; } //Optional, downloadable file url
        public string content { get; set; } //Optional, Raw content, will be used when type is content
        // TODO: wrong documented in the moodle api, it can be null, so it's Optional
        // because we don't know if there are other keys that can be null, we decided
        // to add the nullable ? to every int and every bool to make sure the parser
        // doesn't crash
        public long? timecreated { get; set; } //Time created
        public long? timemodified { get; set; } //Time modified
        public int? sortorder { get; set; } //Content sort order
        public string mimetype { get; set; } //Optional, File mime type.
        // TODO: wrong documented in the moodle api, it's bool not int
        public bool? isexternalfile { get; set; } //Optional, Whether is an external file.
        public string repositorytype { get; set; } //Optional, The repository type for external files.
        // TODO: wrong documented in the moodle api, it can be null, so it's Optional
        public int? userid { get; set; } //User who added this content to moodle
        public string author { get; set; } //Content owner
        public string license { get; set; } //Content license

        /// <summary>
        /// Default constructor
        /// </summary>
        public CourseContentModuleContentExternal()
        { }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="copy">Object that gets copied</param>
        public CourseContentModuleContentExternal(CourseContentModuleContentExternal copy)
        {
            type = copy.type;
            filename = copy.filename;
            filepath = copy.filepath;
            filesize = copy.filesize;
            fileurl = copy.fileurl;
            content = copy.content;
            timecreated = copy.timecreated;
            timemodified = copy.timemodified;
            sortorder = copy.sortorder;
            mimetype = copy.mimetype;
            isexternalfile = copy.isexternalfile;
            repositorytype = copy.repositorytype;
            userid = copy.userid;
            author = copy.author;
            license = copy.license;
        }
    }

    public class CourseContentModuleExternal
    {

        public int? id { get; set; } //activity id
        public string url { get; set; } //Optional, activity url
        public string name { get; set; } //activity module name
        public int? instance { get; set; } //Optional, instance id
        public string description { get; set; } //Optional, activity description
        public int? visible { get; set; } //Optional, is the module visible
        public bool? uservisible { get; set; } //Optional, Is the module visible for the user?
        public string availabilityinfo { get; set; } //Optional, Availability information.
        public int? visibleoncoursepage { get; set; } //Optional, is the module visible on course page
        public string modicon { get; set; } //activity icon url
        public string modname { get; set; } //activity module type
        public string modplural { get; set; } //activity module plural name
        public string availability { get; set; } //Optional, module availability settings
        public int? indent { get; set; } //number of identation in the site
        public List<CourseContentModuleContentExternal> contents { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CourseContentModuleExternal()
        { }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="copy">Object that gets copied</param>
        public CourseContentModuleExternal(CourseContentModuleExternal copy)
        {
            id = copy.id;
            url = copy.url;
            //url = MoodleApi.CutUri(copy.url);
            name = copy.name;
            instance = copy.instance;
            description = copy.description;
            visible = copy.visible;
            uservisible = copy.uservisible;
            availabilityinfo = copy.availabilityinfo;
            visibleoncoursepage = copy.visibleoncoursepage;
            modicon = copy.modicon;
            modname = copy.modname;
            modplural = copy.modplural;
            availability = copy.availability;
            indent = copy.indent;
            contents = new List<CourseContentModuleContentExternal>();
            copy.contents?.ForEach((item) =>
                {
                    contents.Add(new CourseContentModuleContentExternal(item));
                }
            );
        }
    }

    public class CourseContentExternal
    {

        public int? id { get; set; } //Section ID
        public string name { get; set; } //Section name
        public int? visible { get; set; } //Optional, is the section visible
        public string summary { get; set; } //Section description
        public int? summaryformat { get; set; } //summary format (1 = HTML, 0 = MOODLE, 2 = PLAIN or 4 = MARKDOWN)
        public int? section { get; set; } //Optional, Section number inside the course
        public int? hiddenbynumsections { get; set; } //Optional, Whether is a section hidden in the course format
        //TODO: why do i receive a bool instead of an int
        public bool? uservisible { get; set; } //Optional, Is the section visible for the user?
        public string availabilityinfo { get; set; } //Optional, Availability information.
        public List<CourseContentModuleExternal> modules { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CourseContentExternal()
        { }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="copy">Object that gets copied</param>
        public CourseContentExternal(CourseContentExternal copy)
        {
            id = copy.id;
            name = copy.name;
            visible = copy.visible;
            summary = copy.summary;
            summaryformat = copy.summaryformat;
            section = copy.section;
            hiddenbynumsections = copy.hiddenbynumsections;
            uservisible = copy.uservisible;
            availabilityinfo = copy.availabilityinfo;
            modules = new List<CourseContentModuleExternal>();
            copy.modules.ForEach((item) =>
                {
                    modules.Add(new CourseContentModuleExternal(item));
                }
            );
        }
    }
}
