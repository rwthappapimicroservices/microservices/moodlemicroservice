using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Web;
using Newtonsoft.Json;
using PIT.Labs.RestClientLib;
using PIT.Labs.ApiResponseLib;

namespace PIT.Labs.RWTHApp.DataSources.Moodle
{
    public class MoodleApi: IMoodleApi
    {
        private IRestClient restClient = new RestClient();

        /// <summary>
        /// The uri to the moodle service.
        /// </summary>
        public static readonly string moodle_uri = System.Environment.GetEnvironmentVariable("MOODLE_URL");

        /// <summary>
        /// The username of the ext_token_login plugin.
        /// </summary>
        /// <value>Is needed to login into moodle with an external token and to get the normal moodleToken.</value>
        private static readonly string ext_token_login_user = System.Environment.GetEnvironmentVariable("MOODLE_EXT_TOKEN_LOGIN_USER");

        /// <summary>
        /// The name of the moodle webservice.
        /// </summary>
        /// <value>The webservice name is used during the login.</value>
        private static readonly string webservice = System.Environment.GetEnvironmentVariable("MOODLE_WEBSERVICE");

        /// <summary>
        /// The Token from the Moodle RWTHApp Admin User
        /// </summary>
        /// <value>The webservice name is used during the login.</value>
        private static readonly string moodle_admin_token = System.Environment.GetEnvironmentVariable("MOODLE_ADMIN_TOKEN");

        /// <summary>
        /// The uri where we can find the moodle webservice api.
        /// </summary>
        /// <value>Should only be changed if moodle changes it's webservice structure.</value>
        private static readonly string webservice_uri = moodle_uri + "webservice/rest/server.php";

        /// <summary>
        /// The uri to login the user.
        /// </summary>
        /// <value>Should only be changed if moodle changes it's login structure.</value>
        private static readonly string webservice_login_uri = moodle_uri + "login/token.php";

        /// <summary>
        /// The uri used to download files.
        /// </summary>
        /// <value>Should only be changed if moodle changes it's file download structure</value>
        public static readonly string downloadfile_part = "webservice/pluginfile.php";

        /// <summary>
        /// Checks if the MoodleResponse contains an error or an exception.
        /// </summary>
        /// <exception cref="MoodleError">The MoodleResponse contains an error or exception.</exception>
        private void maybeThrowException(MoodleResponse response)
        {
            if (!String.IsNullOrEmpty(response.errorcode))
                throw new MoodleError(response);
        }

        private void testIfMoodleMaintenance(Exception e, string text)
        {
            //try to deserialize  given text to be sure its no json
            try
            {
                dynamic deserialized = JsonConvert.DeserializeObject(text);
            }
            catch (Newtonsoft.Json.JsonReaderException eInner)
            {
                //if its no json +"wartungsarbeiten" -> Moodle maintenance   
                if (text.ToLower().Contains("wartungsarbeiten"))
                {
                    throw new DataSourceException("Moodle ist in Wartung", 4503);
                }
                throw e;
            }
        }

        public T Deserialize<T>(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return default(T);
            }
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(s);
        }

        /// <summary>
        /// Tries to login and get an moodle webservice token.
        /// </summary>
        /// <remarks>It tries to login a user with the ext_token_plugin login function.</remarks>
        /// <exception cref="MoodleError">The MoodleResponse contains an error or exception.</exception>
        public string extTokenLogin(string oauth_token)
        {
            var x = webservice_login_uri;
            var uri = new Uri(webservice_login_uri);
            NameValueCollection query = HttpUtility.ParseQueryString(string.Empty);
            // Add endpoint specific parameters
            query.Add("username", ext_token_login_user);
            query.Add("password", "");
            query.Add("token", oauth_token);
            query.Add("service", webservice);
            LoginResponse response = null;
            string text = restClient.HttpText("GET", uri, query.ToString());
            //test if there is a moodle maintenance
            try
            {
                response = Deserialize<LoginResponse>(text);//restClient.HttpGetJson<LoginResponse>(uri, query.ToString());
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                testIfMoodleMaintenance(e, text);
                throw e;
            }
            maybeThrowException(response);
            return response.token;
        }

        /// <summary>
        /// Calls a function on the moodle webservice api, tries to parse the result into T and returns the result
        /// </summary>
        /// <exception cref="MoodleError">The MoodleResponse contains an error or exception.</exception>
        private T restCall<T> (string wsfunction, string moodle_token, string form_params=null) where T : MoodleResponse
        {
            Console.WriteLine("Request URL:");
            Console.WriteLine(webservice_uri);
            var uri = new Uri(webservice_uri);
            NameValueCollection query = HttpUtility.ParseQueryString(string.Empty);
            query.Add("wstoken", moodle_token);
            query.Add("wsfunction", wsfunction);
            query.Add("moodlewsrestformat", "json");

            T response;
            string text = restClient.HttpText("POST", uri, query.ToString(), form_params, null, null, "application/x-www-form-urlencoded; charset=utf8");

            try
            {
                response = Deserialize<T>(text);
            } catch (Newtonsoft.Json.JsonSerializationException e)
            {
                var res = Deserialize<MoodleResponse>(text);
                var err = new MoodleError(res);
                throw err;
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                testIfMoodleMaintenance(e, text);
                throw e;
            }
            maybeThrowException(response);
            return response;
        }

        /// <summary>
        /// Calls a function on the moodle webservice api with the rwth moodle api admin user
        /// tries to parse the result into T and returns the result
        /// </summary>
        /// <exception cref="MoodleError">The MoodleResponse contains an error or exception.</exception>
        private T adminRestCall<T> (string wsfunction, string form_params=null) where T : MoodleResponse
        {
            return restCall<T>(wsfunction, moodle_admin_token, form_params);
        }

        public SiteInfoExternal GetSiteInfo(string moodle_token)
        {
            var ret = restCall<SiteInfoExternal>("core_webservice_get_site_info", moodle_token);
            return ret;
        }

        public SiteInfoExternal GetSiteInfoAdmin()
        {
            var ret = adminRestCall<SiteInfoExternal>("core_webservice_get_site_info");
            return ret;
        }

        public MoodleResponseList<CategoryExternal> GetCourseCategories()
        {
            var ret = adminRestCall<MoodleResponseList<CategoryExternal>>("core_course_get_categories");
            return ret;
        }

        public MoodleResponseList<CourseExternal> GetCourses(string moodle_token)
        {
            // throws an exception if restcall fails
            return restCall<MoodleResponseList<CourseExternal>>("core_course_get_courses", moodle_token);
        }

        public MoodleResponseList<CourseExternal> GetCoursesByIds(List<int> courseids, string moodle_token)
        {
            var form_params = ""; // "options[ids][0]=1&options[ids][1]=2"
            for(int i=0; i<courseids.Count; i++)
            {
                if (i > 0)
                {
                    form_params += "&";
                }
                form_params += "options[ids][" + i + "]=" + courseids[i];
            }
            var ret = restCall<MoodleResponseList<CourseExternal>>("core_course_get_courses", moodle_token, form_params);
            return ret;
        }

        public MoodleResponseList<EnrolCourseExternal> GetEnrolledCourses(int user_id, string moodle_token)
        {
            var form_params = "userid=" + user_id;
            // throws an exception if restcall fails
            return restCall<MoodleResponseList<EnrolCourseExternal>>("core_enrol_get_users_courses", moodle_token, form_params);
        }

        public MoodleResponseList<EnrolCourseExternal> GetMyEnrolledCourses(string moodle_token)
        {
            var site_info = GetSiteInfo(moodle_token);
            var user_id = site_info.userid;
            // throws an exception if restcall fails
            return GetEnrolledCourses(user_id, moodle_token);
        }

        public static string CutDownloadFileUri(string fileurl)
        {
            var hostname = MoodleApi.downloadfile_part;
            var index = fileurl.IndexOf(hostname);
            if (index > 0)
            {
                var start = index + hostname.Length;
                return fileurl.Substring(start);
            } else
            {
                return "";
            }
        }

        public MoodleResponseList<CourseContentExternal> GetCourseContents(int courseid, string moodle_token)
        {
            var form_params = "courseid=" + courseid;
            var ret = restCall<MoodleResponseList<CourseContentExternal>>("core_course_get_contents", moodle_token, form_params);
            return ret;
        }

        public MoodleResponseList<CourseUserProfileExternal> GetMyRolesById(int userid, int courseid, string moodle_token)
        {
            var form_params = "userlist[0][userid]=" + userid
                + "&userlist[0][courseid]=" + courseid;
            var ret = restCall<MoodleResponseList<CourseUserProfileExternal>>("core_user_get_course_user_profiles", moodle_token, form_params);
            return ret;
        }

        public Stream DownloadFile(string filename, string downloadurl, string moodle_token)
        {
            var link = moodle_uri + downloadfile_part + downloadurl;
            var ret = GetStream(link, moodle_token);
            return ret;
        }

        private Stream GetStream(string downloadurl, string moodle_token)
        {
            var uriBuilder = new UriBuilder(downloadurl);
            NameValueCollection query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["token"] = moodle_token;
            uriBuilder.Query = query.ToString();
            return restClient.HttpGetRaw(uriBuilder.Uri, String.Empty);
        }

        public string GetSemester(int semester_offset)
        {
            var semester_url = Environment.GetEnvironmentVariable("SEMESTER_URL");
            semester_url += "AktuellesSemester";
            semester_url += "?semesterOffset=" + semester_offset;
            var uri = new Uri(semester_url); // http://rwth_semester/AktuellesSemester?semesterOffset=0
            string text = restClient.HttpGetText(uri, "");
            ApiResponse<string> response = Deserialize<ApiResponse<string>>(text);
            if (String.IsNullOrEmpty(response.Data))
            {
                throw new Exception("Invalid semester offset.");
            }
            return response.Data;
        }

    }
}
