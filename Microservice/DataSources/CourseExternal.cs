using System.Collections.Generic;

namespace PIT.Labs.RWTHApp.DataSources.Moodle
{
    public class CourseFormatOptionExternal
    {
        public string name { get; set; }    // course format option name
        public string value { get; set; }   // course format option value

        public CourseFormatOptionExternal()
        { }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="copy">Object that gets copied</param>
        public CourseFormatOptionExternal(CourseFormatOptionExternal copy)
        {
            name = copy.name;
            value = copy.value;
        }
    }

    public class CourseExternal
    {
        public int? id { get; set; }                  // course id
        public string shortname { get; set; }         // course short name
        public int? categoryid { get; set; }          // category id
        public int? categorysortorder { get; set; }   // (optional) sort order into the category
        public string fullname { get; set; }          // full name
        public string displayname { get; set; }       // course display name
        public string idnumber { get; set; }          // (optional) id number
        public string summary { get; set; }           // summary
        public int? summaryformat { get; set; }       // summary format (1 = HTML, 0 = MOODLE, 2 = PLAIN or 4 = MARKDOWN)
        public string format { get; set; }            // course format: weeks, topics, social, site,..
        public int? showgrades { get; set; }          // (optional) 1 if grades are shown, otherwise 0
        public int? newsitems { get; set; }           // (optional) number of recent items appearing on the course page
        public long? startdate { get; set; }           // timestamp when the course start
        public long? enddate { get; set; }             // timestamp when the course end
        public int? numsections { get; set; }         // (optional) (deprecated, use courseformatoptions) number of weeks/topics
        public int? maxbytes { get; set; }            // (optional) largest size of file that can be uploaded into the course
        public int? showreports { get; set; }         // (optional) are activity report shown (yes = 1, no =0)
        public int? visible { get; set; }             // (optional) 1: available to student, 0:not available
        public int? hiddensections { get; set; }      // (optional) (deprecated, use courseformatoptions) How the hidden sections in the course are displayed to students
        public int? groupmode { get; set; }           // (optional) no group, separate, visible
        public int? groupmodeforce { get; set; }      // (optional) 1: yes, 0: no
        public int? defaultgroupingid { get; set; }   // (optional) default grouping id
        public long? timecreated { get; set; }         // (optional) timestamp when the course have been created
        public long? timemodified { get; set; }        // (optional) timestamp when the course have been modified
        public int? enablecompletion { get; set; }    // (optional) Enabled, control via completion and activity settings. Disbaled, not shown in activity settings.
        public int? completionnotify { get; set; }    // (optional) 1: yes 0: no
        public string lang { get; set; }             // (optional) forced course language
        public string forcetheme { get; set; }       // (optional) name of the force theme
        public List<CourseFormatOptionExternal> courseformatoptions { get; set; } //Optional, additional options for particular course format

        /// <summary>
        /// Default constructor
        /// </summary>
        public CourseExternal()
        { }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="copy">Object that gets copied</param>
        public CourseExternal(CourseExternal copy)
        {
            id = copy.id;
            shortname = copy.shortname;
            categoryid = copy.categoryid;
            categorysortorder = copy.categorysortorder;
            fullname = copy.fullname;
            displayname = copy.displayname;
            idnumber = copy.idnumber;
            summary = copy.summary;
            summaryformat = copy.summaryformat;
            format = copy.format;
            showgrades = copy.showgrades;
            newsitems = copy.newsitems;
            startdate = copy.startdate;
            enddate = copy.enddate;
            numsections = copy.numsections;
            maxbytes = copy.maxbytes;
            showreports = copy.showreports;
            visible = copy.visible;
            hiddensections = copy.hiddensections;
            groupmode = copy.groupmode;
            groupmodeforce = copy.groupmodeforce;
            defaultgroupingid = copy.defaultgroupingid;
            timecreated = copy.timecreated;
            timemodified = copy.timemodified;
            enablecompletion = copy.enablecompletion;
            completionnotify = copy.completionnotify;
            lang = copy.lang;
            forcetheme = copy.forcetheme;
            courseformatoptions = new List<CourseFormatOptionExternal>();
            copy.courseformatoptions.ForEach((item) =>
                {
                    courseformatoptions.Add(new CourseFormatOptionExternal(item));
                }
            );
        }

    }

    public class EnrolCourseExternal
    {
        public int id { get; set; }                 // id of course
        public string shortname { get; set; }       // short name of course
        public string fullname { get; set; }        // long name of course
        public int enrolledusercount { get; set; }  // Number of enrolled users in this course
        public string idnumber { get; set; }        // id number of course
        public int visible { get; set; }            // 1 means visible, 0 means hidden course
        public string summary { get; set; }         // (optional) summary
        public int? summaryformat { get; set; }     // (optional) summary format (1 = HTML, 0 = MOODLE, 2 = PLAIN or 4 = MARKDOWN)
        public string format { get; set; }          // (optional) course format: weeks, topics, social, site
        public bool? showgrades { get; set; }       // (optional) true if grades are shown, otherwise false
        public string lang { get; set; }            // (optional) forced course language
        public bool? enablecompletion { get; set; } // (optional) true if completion is enabled, otherwise false
        public int? category { get; set; }          // (optional) course category id
        public double? progress { get; set; }       // (optional) Progress percentage
        public long? startdate { get; set; }         // (optional) Timestamp when the course start
        public long? enddate { get; set; }           // (optional) Timestamp when the course end

        public EnrolCourseExternal()
        { }

        public EnrolCourseExternal(EnrolCourseExternal copy)
        {
            id = copy.id;
            shortname = copy.shortname;
            fullname = copy.fullname;
            enrolledusercount = copy.enrolledusercount;
            idnumber = copy.idnumber;
            visible = copy.visible;
            summary = copy.summary;
            summaryformat = copy.summaryformat;
            format = copy.format;
            showgrades = copy.showgrades;
            lang = copy.lang;
            enablecompletion = copy.enablecompletion;
            category = copy.category;
            progress = copy.progress;
            startdate = copy.startdate;
            enddate = copy.enddate;
        }
    }

}
