using System.Collections.Generic;

namespace PIT.Labs.RWTHApp.DataSources.Moodle
{
    public class CourseUserProfileExternal
    {
        public int id;
        public List<RoleExternal> roles;

        public CourseUserProfileExternal()
        { }

        public CourseUserProfileExternal(CourseUserProfileExternal copy)
        {
            this.id = copy.id;
            this.roles = new List<RoleExternal>();
            copy.roles.ForEach((item) =>
                {
                    this.roles.Add(new RoleExternal(item));
                }
            );
        }
    }

    public class RoleExternal
    {
        public int roleid;
        public string name;
        public string shortname;
        public int sortorder;

        public RoleExternal()
        { }

        public RoleExternal(RoleExternal copy)
        {
            this.roleid = copy.roleid;
            this.name = copy.name;
            this.shortname = copy.shortname;
            this.sortorder = copy.sortorder;
        }
    }
}
