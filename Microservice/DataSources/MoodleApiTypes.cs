using System;
using System.Collections.Generic;

namespace PIT.Labs.RWTHApp.DataSources.Moodle
{
    public class DataSourceException: Exception
    {
        public int ErrorCode {get;set;}

        public DataSourceException()
        {
        }

        public DataSourceException(string message)
            : base(message)
        {
        }

        public DataSourceException(string message, int error_code)
            : base(message)
        {
            this.ErrorCode = error_code;
        }
    }

    public class MoodleError: DataSourceException
    {
        private static Dictionary<string, int> error_mapper = new Dictionary<string, int>() {
            { "unknown", 0 },
            { "backupprecheckerrors", 1 },
            { "cannotcreatetoken", 2 },
            { "cannotgetcoursecontents", 3 },
            { "docaccessrefused", 4 },
            { "enablewsdescription", 5 },
            { "errorcatcontextnotvalid", 6 },
            { "errorcoursecontextnotvalid", 7 },
            { "errorinvalidparam", 8 },
            { "errornotemptydefaultparamarray", 9 },
            { "erroroptionalparamarray", 10 },
            { "forbiddenwsuser", 11 },
            { "formatnotsupported", 12 },
            { "installexistingserviceshortnameerror", 13 },
            { "installserviceshortnameerror", 14 },
            { "invalidextparam", 15 },
            { "invalidiptoken", 16 },
            { "invalidtimedtoken", 17 },
            { "invalidlogin", 1 },
            { "invalidparameter", 1 },
            { "invalidtoken", 18 },
            { "missingpassword", 19 },
            { "missingparam", 20 },
            { "missingrequiredcapability", 21 },
            { "missingusername", 22 },
            { "missingversionfile", 23 },
            { "passwordisexpired", 24 },
            { "restoredaccountresetpassword", 25 },
            { "servicenotavailable", 26 },
            { "unnamedstringparam", 27 },
            { "usernotallowed", 28 },
            { "wrongusernamepassword", 29 },
            { "wsaccessuserunconfirmed", 30 },
        };

        public static string GetMessage(MoodleResponse error)
        {
            string message = error.error??error.message;
            string moodle_errorcode = error.errorcode;
            return error_mapper.ContainsKey(moodle_errorcode) ? message : message+" (unknown errorcode: "+moodle_errorcode+")";
        }

        public static int GetErrorCode(MoodleResponse error)
        {
            string moodle_errorcode = error.errorcode;
            return error_mapper.ContainsKey(moodle_errorcode) ? error_mapper[moodle_errorcode] : error_mapper["unknown"];
        }

        public MoodleError(MoodleResponse error): base(GetMessage(error), GetErrorCode(error))
        {
        }
    }

    /// <summary>
    /// This class is a base class for all moodle responses. 
    /// </summary>
    /// <remarks>
    /// It contains the attributes for moodle errors and exceptions,
    /// because they can always be returned from moodle.
    /// Errors are thrown for example if the login fails.
    /// Exceptions are thrown for example if you try to execute a webservice function that doesn't exist.
    /// </remarks>
    public class MoodleResponse
    {

        /// <summary>
        /// A description of an moodle error, language dependend
        /// </summary>
        /// <example>
        /// This attribute can be for example one of the following strings:
        /// "A required parameter (username) was missing"
        /// </example>
        public string error { get; set; }

        /// <summary>
        /// A description of an moodle exception, language independend
        /// </summary>
        /// <example>
        /// This attribute can be for example one of the following strings:
        /// "invalid_parameter_exception"
        /// </example>
        public string exception { get; set; }

        /// <summary>
        /// A description of an moodle exception, language dependend
        /// </summary>
        /// <example>
        /// This attribute can be for example one of the following strings:
        /// "Invalid parameter value detected"
        /// </example>
        public string message { get; set; }

        /// <summary>
        /// A code that identifies the problem
        /// </summary>
        /// <example>
        /// This attribute can be for example one of the following strings:
        /// "missingparam", "invalidlogin", "servicenotavailable", "cannotcreatetoken"
        /// </example>
        public string errorcode { get; set; }

        public string stacktrace { get; set; }

        public string debuginfo { get; set; }

        public string reproductionlink { get; set; }

        public MoodleResponse()
        { }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="copy">Object that gets copied</param>
        public MoodleResponse(MoodleResponse copy)
        {
            error = copy.error;
            exception = copy.exception;
            message = copy.message;
            errorcode = copy.errorcode;
            stacktrace = copy.stacktrace;
            debuginfo = copy.debuginfo;
            reproductionlink = copy.reproductionlink;
        }
    }

    /// <summary>
    /// A moodle response that is a list with objects
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MoodleResponseList<T> : MoodleResponse, IList<T>
    {
        public List<T> TList
        { get; private set; }

        public int Count => ((IList<T>)TList).Count;

        public bool IsReadOnly => ((IList<T>)TList).IsReadOnly;

        public T this[int index] { get { return ((IList<T>)TList)[index]; } set { ((IList<T>)TList)[index] = value; } }

        public MoodleResponseList()
        {
            TList = new List<T>();
        }

        public void Add(T item)
        {
            TList.Add(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return TList.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int IndexOf(T item)
        {
            return ((IList<T>)TList).IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            ((IList<T>)TList).Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            ((IList<T>)TList).RemoveAt(index);
        }

        public void Clear()
        {
            ((IList<T>)TList).Clear();
        }

        public bool Contains(T item)
        {
            return ((IList<T>)TList).Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            ((IList<T>)TList).CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            return ((IList<T>)TList).Remove(item);
        }
    }

    /// <summary>
    /// The webservice login response from moodle login/token.php
    /// </summary>
    public class LoginResponse: MoodleResponse
    {
        public string token { get; set; }
        public string privatetoken { get; set; }

        public LoginResponse()
        { }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="copy">Object that gets copied</param>
        public LoginResponse(LoginResponse copy): base(copy)
        {
            token = copy.token;
            privatetoken = copy.privatetoken;
        }

        /// <summary>
        /// Cast Constructor
        /// </summary>
        /// <param name="cast">Base object to cast into</param>
        public LoginResponse(MoodleResponse cast): base(cast)
        { }
    }

}
