namespace PIT.Labs.RWTHApp.DataSources.Moodle
{
    public class CategoryExternal
    {
        public int id { get; set; }                 // category id
        public string name { get; set; }            // category name
        public string idnumber { get; set; }        // Optional category id number
        public string description { get; set; }     // category description
        public int descriptionformat { get; set; }  // description format (1 = HTML, 0 = MOODLE, 2 = PLAIN or 4 = MARKDOWN)
        public int parent { get; set; }             // parent category id
        public int sortorder { get; set; }          // category sorting order
        public int coursecount { get; set; }        // number of courses in this category
        public int? visible { get; set; }           // Optional 1: available, 0:not available
        public int? visibleold { get; set; }        // Optional 1: available, 0:not available
        public long? timemodified { get; set; }      // Optional timestamp
        public int depth { get; set; }              // category depth
        public string path { get; set; }            // category path
        public string theme { get; set; }           // Optional category theme

        public CategoryExternal()
        { }

        public CategoryExternal(CategoryExternal copy)
        {
            this.id = copy.id;
            this.name = copy.name;
            this.idnumber = copy.idnumber;
            this.description = copy.description;
            this.descriptionformat = copy.descriptionformat;
            this.parent = copy.parent;
            this.sortorder = copy.sortorder;
            this.coursecount = copy.coursecount;
            this.visible = copy.visible;
            this.visibleold = copy.visibleold;
            this.timemodified = copy.timemodified;
            this.depth = copy.depth;
            this.path = copy.path;
            this.theme = copy.theme;
        }
    }
}
