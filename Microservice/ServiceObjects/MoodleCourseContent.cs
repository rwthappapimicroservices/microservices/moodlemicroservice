using System.Collections.Generic;
using PIT.Labs.RWTHApp.DataSources.Moodle;

namespace PIT.Labs.RWTHApp.ProxyService.ServiceObjects
{
    public class MoodleCourseContentModuleContent
    {
        public string type { get; set; } //a file or a folder or external link
        public string filename { get; set; } //filename
        public string filepath { get; set; } //filepath
        public int? filesize { get; set; } //filesize
        public string fileurl { get; set; } //Optional, downloadable file url
        public string content { get; set; } //Optional, Raw content, will be used when type is content
        public long? timecreated { get; set; } //Time created
        public long? timemodified { get; set; } //Time modified
        public int? sortorder { get; set; } //Content sort order
        public string mimetype { get; set; } //Optional, File mime type.
        public bool? isexternalfile { get; set; } //Optional, Whether is an external file.
        public string repositorytype { get; set; } //Optional, The repository type for external files.
        public int? userid { get; set; } //User who added this content to moodle
        public string author { get; set; } //Content owner
        public string license { get; set; } //Content license

        /// <summary>
        /// Default constructor
        /// </summary>
        public MoodleCourseContentModuleContent()
        { }

        /// <summary>
        /// Cast constructor
        /// </summary>
        /// <param name="cast">Object that gets casted</param>
        public MoodleCourseContentModuleContent(CourseContentModuleContentExternal cast)
        {
            type = cast.type;
            filename = cast.filename;
            filepath = cast.filepath;
            filesize = cast.filesize;
            fileurl = cast.fileurl;
            /* fileurl = MoodleApi.CutDownloadFileUri(cast.fileurl); */
            content = cast.content;
            timecreated = cast.timecreated;
            timemodified = cast.timemodified;
            sortorder = cast.sortorder;
            mimetype = cast.mimetype;
            isexternalfile = cast.isexternalfile;
            repositorytype = cast.repositorytype;
            userid = cast.userid;
            author = cast.author;
            license = cast.license;
        }
    }

    public class MoodleCourseContentModule
    {

        public int? id { get; set; } //activity id
        public string url { get; set; } //Optional, activity url
        public string name { get; set; } //activity module name
        public int? instance { get; set; } //Optional, instance id
        public string description { get; set; } //Optional, activity description
        public int? visible { get; set; } //Optional, is the module visible
        public bool? uservisible { get; set; } //Optional, Is the module visible for the user?
        public string availabilityinfo { get; set; } //Optional, Availability information.
        public int? visibleoncoursepage { get; set; } //Optional, is the module visible on course page
        public string modicon { get; set; } //activity icon url
        public string modname { get; set; } //activity module type
        public string modplural { get; set; } //activity module plural name
        public string availability { get; set; } //Optional, module availability settings
        public int? indent { get; set; } //number of identation in the site
        public List<MoodleCourseContentModuleContent> contents { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public MoodleCourseContentModule()
        { }

        /// <summary>
        /// Cast constructor
        /// </summary>
        /// <param name="cast">Object that gets casted</param>
        public MoodleCourseContentModule(CourseContentModuleExternal cast)
        {
            id = cast.id;
            url = cast.url;
            name = cast.name;
            instance = cast.instance;
            description = cast.description;
            visible = cast.visible;
            uservisible = cast.uservisible;
            availabilityinfo = cast.availabilityinfo;
            visibleoncoursepage = cast.visibleoncoursepage;
            modicon = cast.modicon;
            modname = cast.modname;
            modplural = cast.modplural;
            availability = cast.availability;
            indent = cast.indent;
            contents = new List<MoodleCourseContentModuleContent>();
            cast.contents?.ForEach((item) =>
                {
                    contents.Add(new MoodleCourseContentModuleContent(item));
                }
            );
        }
    }

    public class MoodleCourseContent
    {

        public int? id { get; set; } //Section ID
        public string name { get; set; } //Section name
        public int? visible { get; set; } //Optional, is the section visible
        public string summary { get; set; } //Section description
        public int? summaryformat { get; set; } //summary format (1 = HTML, 0 = MOODLE, 2 = PLAIN or 4 = MARKDOWN)
        public int? section { get; set; } //Optional, Section number inside the course
        public int? hiddenbynumsections { get; set; } //Optional, Whether is a section hidden in the course format
        public bool? uservisible { get; set; } //Optional, Is the section visible for the user?
        public string availabilityinfo { get; set; } //Optional, Availability information.
        public List<MoodleCourseContentModule> modules { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public MoodleCourseContent()
        { }

        /// <summary>
        /// Cast constructor
        /// </summary>
        /// <param name="cast">Object that gets casted</param>
        public MoodleCourseContent(CourseContentExternal cast)
        {
            id = cast.id;
            name = cast.name;
            visible = cast.visible;
            summary = cast.summary;
            summaryformat = cast.summaryformat;
            section = cast.section;
            hiddenbynumsections = cast.hiddenbynumsections;
            uservisible = cast.uservisible;
            availabilityinfo = cast.availabilityinfo;
            modules = new List<MoodleCourseContentModule>();
            cast.modules.ForEach((item) =>
                {
                    modules.Add(new MoodleCourseContentModule(item));
                }
            );
        }
    }
}
