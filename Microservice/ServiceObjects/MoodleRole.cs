using System.Collections.Generic;
using PIT.Labs.RWTHApp.DataSources.Moodle;

namespace PIT.Labs.RWTHApp.ProxyService.ServiceObjects
{
    public class MoodleRole
    {
        public int roleid;
        public string name;
        public string shortname;
        public int sortorder;

        public MoodleRole()
        { }

        public MoodleRole(RoleExternal cast)
        {
            this.roleid = cast.roleid;
            this.name = cast.name;
            this.shortname = cast.shortname;
            this.sortorder = cast.sortorder;
        }
    }

    public class MoodleRoleList
    {
        public List<MoodleRole> roles;

        public MoodleRoleList()
        { }

        public MoodleRoleList(List<MoodleRole> roles)
        {
            this.roles = roles;
        }
    }
}