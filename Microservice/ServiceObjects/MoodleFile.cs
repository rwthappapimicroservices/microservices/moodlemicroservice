using System.Web;
using PIT.Labs.RWTHApp.DataSources.Moodle;

namespace PIT.Labs.RWTHApp.ProxyService.ServiceObjects
{

    public class MoodleFileInformation
    {
        public string mimetype { get; set; }
        public int filesize { get; set; }

        public MoodleFileInformation()
        { }

        public MoodleFileInformation(string mimetype, int filesize)
        {
            this.mimetype = mimetype;
            this.filesize = filesize;
        }
    }

    public class MoodleFile
    {
        public string filename { get; set; }
        public string modulename { get; set; }
        public string topicname { get; set; }
        public string sourceDirectory { get; set; }
        public long? created { get; set; }
        public long? lastModified { get; set; }
        public string selfUrl { get; set; }
        public string downloadUrl { get; set; }
        public MoodleFileInformation fileinformation { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public MoodleFile()
        { }

        /// <summary>
        /// Cast constructor
        /// </summary>
        /// <param name="cast">Object that gets casted</param>
        public MoodleFile(CourseContentModuleContentExternal content, CourseContentModuleExternal module, CourseContentExternal topic)
        {
            filename = content.filename;
            modulename = HttpUtility.HtmlDecode(module.name);
            topicname = HttpUtility.HtmlDecode(topic.name);
            sourceDirectory = content.filepath;
            //TODO check if it is realy "directory"
            created = content.timecreated;
            lastModified = content.timemodified;

            // convert fileurl
            if (content.fileurl != null)
            { 
                /* var converted_url = MoodleApi.CutDownloadFileUri(content.fileurl); */
                var converted_url = content.fileurl;
                selfUrl = converted_url;
                downloadUrl = converted_url;
            }

            fileinformation = new MoodleFileInformation(content.mimetype, content.filesize??0);
        }

    }

}
