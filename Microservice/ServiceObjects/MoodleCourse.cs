using System.Web;
using PIT.Labs.RWTHApp.DataSources.Moodle;

namespace PIT.Labs.RWTHApp.ProxyService.ServiceObjects
{

    public class MoodleCourseCategory
    {
        public string name { get; set; }
        public int id { get; set; }
        public string idnumber { get; set; }

        public MoodleCourseCategory()
        { }

        public MoodleCourseCategory(CategoryExternal cast)
        {
            this.id = cast.id;
            this.name = cast.name;
            this.idnumber = cast.idnumber;
        }
    }

    public class MoodleCourse
    {
        public int? id { get; set; }                  // course id
        public string shortName { get; set; }         // course short name
        public MoodleCourseCategory category { get; set; }    // semester
        public string courseTitle { get; set; }       // course display name
        public string description { get; set; }       // summary
        public string url { get; set; }               // "https://livemoodle.rwth-aachen.de/hier/gehts/zum/Kurs"
        public long? startDate { get; set; }           // timestamp when the course start
        public long? endDate { get; set; }             // timestamp when the course end
        public long? timeModified { get; set; }        // (optional) timestamp when the course has been modified
        public string lvnr { get; set; }          // Its an EventID but its not unique, can return in different semester

        /// <summary>
        /// Default constructor
        /// </summary>
        public MoodleCourse()
        { }

        public static string GenerateCourseUrl(int? course_id)
        {
            if (course_id == null)
            {
                return "";
            }
            var moodle_url = System.Environment.GetEnvironmentVariable("MOODLE_URL");
            return moodle_url + "course/view.php?id=" + course_id;
        }

        /// <summary>
        /// CourseExternal Cast constructor
        /// </summary>
        /// <param name="cast">Object that gets casted</param>
        public MoodleCourse(CourseExternal cast, CategoryExternal category)
        {
            this.id = cast.id;
            this.shortName = cast.shortname;
            this.category = new MoodleCourseCategory(category);
            this.courseTitle = HttpUtility.HtmlDecode(cast.displayname);
            this.description = cast.summary;
            this.url = GenerateCourseUrl(cast.id);
            this.startDate = cast.startdate;
            this.endDate = cast.enddate;
            this.timeModified = cast.timemodified;
            this.lvnr = cast.idnumber;
        }

        /// <summary>
        /// EnrolCourseExternal Cast constructor
        /// </summary>
        /// <param name="cast">Object that gets casted</param>
        public MoodleCourse(EnrolCourseExternal cast, CategoryExternal category)
        {
            this.id = cast.id;
            this.shortName = HttpUtility.HtmlDecode(cast.shortname);
            this.category = new MoodleCourseCategory(category);
            this.courseTitle = HttpUtility.HtmlDecode(cast.fullname);
            this.description = cast.summary;
            this.url = GenerateCourseUrl(cast.id);
            this.startDate = cast.startdate;
            this.endDate = cast.enddate;
            this.lvnr = cast.idnumber;
        }

    }
}
