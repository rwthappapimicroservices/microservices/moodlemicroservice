using PIT.Labs.RWTHApp.DataSources.Moodle;
using PIT.Labs.RWTHApp.ProxyService.ServiceObjects;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Net.Http.Headers;

namespace PIT.Labs.RWTHApp.ProxyService.Factory
{
    public class ApiRequestException: DataSourceException
    {
        public ApiRequestException(): base()
        { }

        public ApiRequestException(string message)
            : base(message)
        { }

        public ApiRequestException(string message, int error_code)
            : base(message, error_code)
        { }
    }

    public class MoodleFactory
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static string[] allowedModuleTypesForGetFiles = new string[]
        {
            "resource",
            "folder",
        };

        private IMoodleApi moodleApi;

        public MoodleFactory(IMoodleApi moodleApi)
        {
            this.moodleApi = moodleApi;
        }

        private string login(string oauth_token)
        {
            /* var ret = CacheWrapper.GetCached(() => */
            /*     { */
                    return moodleApi.extTokenLogin(oauth_token);
                    //moodleToken = response.privatetoken ?? response.token;
                /* }, */
                /* ExpirationTime.L, "MoodleLoginToken", oauth_token); */
            /* return ret; */
        }

        private MoodleResponseList<CategoryExternal> GetCategories()
        {
            /* var ret = CacheWrapper.GetCached(() => */
            /*         { */
                        return moodleApi.GetCourseCategories();
                    /* }, */
                    /* // TODO: Prüfen ob token gecached werden muss, oder mit standard moodle webservieuser daten abrufen */
                    /* ExpirationTime.L, "MoodleGetCategories"); */
            /* return ret; */
        }

        private Tuple<bool, CategoryExternal> GetCategory(int? category_id)
        {
            var ext_categories = GetCategories();
            CategoryExternal category = null;
            foreach(var ext_category in ext_categories)
            {
                if (ext_category.id == category_id)
                {
                    category = ext_category;
                }
            }
            bool success = true;
            if (category==null)
            {
                logger.Trace("Couldn't get category id " + category_id + ".");
                category = new CategoryExternal();
                success = false;
            }
            return Tuple.Create(success, category);
        }

        private Tuple<bool, CategoryExternal> GetCategory(string semester)
        {
            var ext_categories = GetCategories();
            CategoryExternal category = null;
            foreach(var ext_category in ext_categories)
            {
                if (ext_category.idnumber == semester)
                {
                    category = ext_category;
                }
            }
            bool success = true;
            if (category==null)
            {
                logger.Trace("Couldn't get category id " + semester + ".");
                category = new CategoryExternal();
                success = false;
            }
            return Tuple.Create(success, category);
        }

        public List<MoodleCourseCategory> GetMoodleCategories()
        {
            var ext_categories = GetCategories();
            var categories = new List<MoodleCourseCategory>();
            foreach (var ext_category in ext_categories)
            {
                var category = new MoodleCourseCategory(ext_category);
                categories.Add(category);
            }
            return categories;
        }

        public List<MoodleCourse> GetMyEnrolledCourses(string oauth_token, string semester, string semester_offset)
        {
            // Semester could be "ss18" and semester_offset the count of previous semesters. For example "0" is the current semester.
            // Only one filter is allowed per call.
            // throw error if both filters are set
            if (!String.IsNullOrEmpty(semester) && !String.IsNullOrEmpty(semester_offset))
            {
                throw new ApiRequestException("You can't use two filters at once.", 103);
            }
            // try parsing semester_offset to semester
            int semester_offset_int;
            if (Int32.TryParse(semester_offset, out semester_offset_int))
            {
                // generate semester name
                semester = moodleApi.GetSemester(semester_offset_int);
            }
            // get category by semester, if there is no category, throw exception
            CategoryExternal ext_category = null;
            if (semester != null)
            {
                var tmp = GetCategory(semester);
                ext_category = tmp.Item2;
                var success = tmp.Item1;
                if(!success) {
                    throw new ApiRequestException("Semester "+semester+" not available.", 104);
                }
            }
            var moodleToken = login(oauth_token);
            var ext_courses = moodleApi.GetMyEnrolledCourses(moodleToken);
            var int_courses = new List<MoodleCourse>();
            foreach(var ext_course in ext_courses)
            {
                // if semester is not null, apply filter
                CategoryExternal category;
                if (ext_category != null && ext_category.id != ext_course.category)
                {
                    continue;
                }
                category = GetCategory(ext_course.category).Item2;
                MoodleCourse int_course = new MoodleCourse(ext_course, category);
                int_courses.Add(int_course);
            }
            return int_courses;
        }

        public MoodleCourse GetMyEnrolledCourseById(string courseid, string oauth_token)
        {
            int courseid_int;
            if (!Int32.TryParse(courseid, out courseid_int))
            {
                // TODO: Add exception type for this
                throw new ApiRequestException("Invalid course id format.", 100);
            }
            var moodleToken = login(oauth_token);
            var ext_courses = moodleApi.GetMyEnrolledCourses(moodleToken);
            foreach(var ext_course in ext_courses)
            {
                if (courseid_int == ext_course.id)
                {
                    CategoryExternal category = GetCategory(ext_course.category).Item2;
                    MoodleCourse int_course = new MoodleCourse(ext_course, category);
                    return int_course;
                }
            }
            throw new ApiRequestException("You are not allowed to see this course or it doesn't exist", 102);
        }

        public MoodleRoleList GetMyRolesById(string courseid, string oauth_token)
        {
            int courseid_int;
            if (!Int32.TryParse(courseid, out courseid_int))
            {
                throw new ApiRequestException("Invalid course id format.", 100);
            }
            var moodleToken = login(oauth_token);
            var site_info = moodleApi.GetSiteInfo(moodleToken);
            var user_id = site_info.userid;
            var ext_contents = moodleApi.GetMyRolesById(user_id, courseid_int, moodleToken);
            List<MoodleRole> roles = new List<MoodleRole>();
            foreach(var userprofile in ext_contents)
            {
                foreach(var role in userprofile.roles)
                {
                    roles.Add(new MoodleRole(role));
                }
            }
            return new MoodleRoleList(roles);
        }

        public List<MoodleFile> GetCourseFiles(string courseid, string topicname, string oauth_token)
        {
            int courseid_int;
            if (!Int32.TryParse(courseid, out courseid_int))
            {
                // TODO: Add exception type for this
                throw new ApiRequestException("Invalid course id format.", 100);
            }
            var moodleToken = login(oauth_token);
            var ext_contents = moodleApi.GetCourseContents(courseid_int, moodleToken);
            List<MoodleFile> int_files = new List<MoodleFile>();
            foreach (var topic in ext_contents)
            {
                // if topicname is null, don't filter
                if (topicname != null && topic.name != topicname)
                {
                    continue;
                }

                topic.modules?.ForEach(module =>
                {
                    module.contents?.ForEach(content =>
                    {
                        // Only include whitelisted module types (files and folders for now)
                        if (allowedModuleTypesForGetFiles.Contains(module.modname))
                        {
                            int_files.Add(new MoodleFile(content, module, topic));
                        }
                    });
                });
            }
            return int_files;
        }

        public string GetContentTypeFromFilename(string filename){
            var provider = new FileExtensionContentTypeProvider();
            string contentType;
            if(!provider.TryGetContentType(filename, out contentType))
            {
                contentType = "application/octet-stream";
            }
            return contentType;
        }

        public FileStreamResult DownloadFile(string filename, string downloadurl, string oauth_token, HttpResponse Response)
        {
            var moodleToken = login(oauth_token);
            var ret = moodleApi.DownloadFile(filename, downloadurl, moodleToken);
            // on unit tests this Current is null
            var contentType = GetContentTypeFromFilename(filename);
            var result = new FileStreamResult(ret, contentType);
            // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Disposition
            // this could replace the redirect to the correct filename, but
            // we have not removed the redirect, because of compatibility issues
            var content_disposition_header = new ContentDispositionHeaderValue("attachment") { }.ToString();
            Response.Headers.Add("Content-Disposition", content_disposition_header);
            return result;
        }

        /*********************************
         * Currently not needed methods  *
         * but keept for future features *
         ********************************/

        private MoodleCourse GetCourseById(string courseid, string oauth_token)
        {
            int courseid_int;
            if (!Int32.TryParse(courseid, out courseid_int))
            {
                // TODO: Add exception type for this
                throw new ApiRequestException("Invalid course id format.", 100);
            }
            var moodleToken = login(oauth_token);
            List<int> courseids = new List<int>();
            courseids.Add(courseid_int);
            var ext_courses = moodleApi.GetCoursesByIds(courseids, moodleToken);
            if (ext_courses.Count <= 0)
            {
                // TODO: Add exception type for this
                throw new ApiRequestException("No course found with id "+courseid+".", 101);
            }
            if (ext_courses.Count > 1)
            {
                // this should not happen, some indexes in moodle are wrong
            }
            CategoryExternal category = GetCategory(ext_courses[0].categoryid).Item2;
            MoodleCourse course = new MoodleCourse(ext_courses[0], category);
            return course;
        }

        private List<MoodleCourseContent> GetCourseContents(string courseid, string oauth_token)
        {
            int courseid_int = -1;
            if (!Int32.TryParse(courseid, out courseid_int))
            {
                // TODO: Add exception type for this
                throw new ApiRequestException("Invalid course id format.", 100);
            }
            var moodleToken = login(oauth_token);
            var ext_contents = moodleApi.GetCourseContents(courseid_int, moodleToken);
            List<MoodleCourseContent> int_contents = new List<MoodleCourseContent>();
            foreach (var topic in ext_contents)
            {
                int_contents.Add(new MoodleCourseContent(topic));
            }
            return int_contents;
        }
	}
}
