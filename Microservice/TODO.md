Factories/MoodleFactory.cs:17
Factories/MoodleFactory.cs:121

Hier war die CampusAPI verlinkt. Die Campus API ist eine andere Domäne, die wird hier benutzt um das Semester abzurufen und einen repräsentativen string zu bekommen. Dieser Semester-String wird benötigt um in Moodle die aktuellen Semester zu filtern.

Damit die Semester nach der Migration noch gefiltert werden können, muss hier entweder die Domäne Campus API in einem separaten Microservice migriert, oder eine Alternative programmiert werden, aus welcher die Semester abgefragt werden können. Da dies in viele anderen Domains auch benötigt wird und somit eine Abhängigkeit von vielen Domänen an die Campus API Domäne entstehen würde, lohnt es sich zu schauen, ob es möglich ist diese Information anders bereit zu stellen um die Domänen zu entkoppeln.
