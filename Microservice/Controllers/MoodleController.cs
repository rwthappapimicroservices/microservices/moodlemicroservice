using System;
using Microsoft.AspNetCore.Mvc;
using PIT.Labs.RWTHApp.ProxyService.ServiceObjects;
using PIT.Labs.RWTHApp.ProxyService.Factory;
using PIT.Labs.RWTHApp.DataSources.Moodle;
using System.Collections.Generic;
using PIT.Labs.ApiResponseLib;

namespace RWTHMoodle.Controllers
{
    [ApiController]
    [Route("eLearning/Moodle/Semester")]
    public class Semester : ControllerBase
    {
        private IMoodleApi moodleApi { get; set; }

        public Semester(IMoodleApi moodleApi)
        {
            this.moodleApi = moodleApi;
        }

        [HttpGet]
        public ActionResult<ApiResponse<string>> Get([FromQuery] string semesterOffset)
        {
            try
            {
                int semester_offset_int;
                if (String.IsNullOrEmpty(semesterOffset))
                {
                    semester_offset_int = 0;
                }
                else
                {
                    if (!Int32.TryParse(semesterOffset, out semester_offset_int))
                    {
                        throw new Exception("Invalid semester offset.");
                    }
                }
                return ApiResponseFactory.Create(moodleApi.GetSemester(semester_offset_int));
            }
            catch (Exception e)
            {
                return ApiResponseFactory.CreateErrorFromException<string>(e);
            }
        }
    }

    [ApiController]
    [Route("eLearning/Moodle/GetCategories")]
    public class GetCategories : ControllerBase
    {
        private MoodleFactory moodleFactory { get; set; }

        public GetCategories(MoodleFactory moodleFactory)
        {
            this.moodleFactory = moodleFactory;
        }

        [HttpGet]
        public ActionResult<ApiResponse<List<MoodleCourseCategory>>> Get()
        {
            try
            {
                var output = moodleFactory.GetMoodleCategories();
                return ApiResponseFactory.Create(output);
            }
            catch (Exception e)
            {
                return ApiResponseFactory.CreateErrorFromException<List<MoodleCourseCategory>>(e);
            }
        }
    }

    [ApiController]
    [Route("eLearning/Moodle/GetMyEnrolledCourses")]
    public class GetMyEnrolledCourses : ControllerBase
    {
        private MoodleFactory moodleFactory { get; set; }

        public GetMyEnrolledCourses(MoodleFactory moodleFactory)
        {
            this.moodleFactory = moodleFactory;
        }

        public ActionResult<ApiResponse<List<MoodleCourse>>> Get([FromQuery] string semester, [FromQuery] string semesterOffset, [FromQuery] string token)
        {
            try
            {
                var output = moodleFactory.GetMyEnrolledCourses(token, semester, semesterOffset);
                return ApiResponseFactory.Create(output);
            }
            catch (Exception e)
            {
                return ApiResponseFactory.CreateErrorFromException<List<MoodleCourse>>(e);
            }
        }
    }

    [ApiController]
    [Route("eLearning/Moodle/GetMyEnrolledCourseById")]
    public class GetMyEnrolledCourseById : ControllerBase
    {
        private MoodleFactory moodleFactory { get; set; }

        public GetMyEnrolledCourseById(MoodleFactory moodleFactory)
        {
            this.moodleFactory = moodleFactory;
        }

        public ActionResult<ApiResponse<MoodleCourse>> Get([FromQuery] string courseid, [FromQuery] string token)
        {
            try
            {
                var output = moodleFactory.GetMyEnrolledCourseById(courseid, token);
                return ApiResponseFactory.Create(output);
            }
            catch (Exception e)
            {
                return ApiResponseFactory.CreateErrorFromException<MoodleCourse>(e);
            }
        }
    }

    [ApiController]
    [Route("eLearning/Moodle/GetMyRolesById")]
    public class GetMyRolesById : ControllerBase
    {
        private MoodleFactory moodleFactory { get; set; }

        public GetMyRolesById(MoodleFactory moodleFactory)
        {
            this.moodleFactory = moodleFactory;
        }

        public ActionResult<ApiResponse<MoodleRoleList>> Get([FromQuery] string courseid, [FromQuery] string token)
        {
            try
            {
                var output = moodleFactory.GetMyRolesById(courseid, token);
                return ApiResponseFactory.Create(output);
            }
            catch (Exception e)
            {
                return ApiResponseFactory.CreateErrorFromException<MoodleRoleList>(e);
            }
        }
    }

    [ApiController]
    [Route("eLearning/Moodle/GetFiles")]
    public class GetFiles : ControllerBase
    {
        private MoodleFactory moodleFactory { get; set; }

        public GetFiles(MoodleFactory moodleFactory)
        {
            this.moodleFactory = moodleFactory;
        }

        public ActionResult<ApiResponse<List<MoodleFile>>> Get([FromQuery] string courseid, [FromQuery] string topicname, [FromQuery] string token)
        {
            try
            {
                var output = moodleFactory.GetCourseFiles(courseid, topicname, token);
                return ApiResponseFactory.Create(output);
            }
            catch (Exception e)
            {
                return ApiResponseFactory.CreateErrorFromException<List<MoodleFile>>(e);
            }
        }
    }

    [ApiController]
    [Route("eLearning/Moodle/DownloadFile/{*filename}")]
    /* [Route("eLearning/Moodle/DownloadFile/{*filename}?downloadurl={downloadurl}&token={token}")] */
    public class DownloadFile : ControllerBase
    {
        private MoodleFactory moodleFactory { get; set; }

        public DownloadFile(MoodleFactory moodleFactory)
        {
            this.moodleFactory = moodleFactory;
        }

        private class RedirectWrapper
        {
            public bool redirected {get; set;}
            public string url {get; set;}
            public RedirectWrapper()
            {
                redirected = false;
            }
            public RedirectWrapper(string _url)
            {
                redirected = true;
                url=_url;
            }
        }

        public IActionResult Get([FromRoute] string filename, [FromQuery] string downloadurl, [FromQuery] string token)
        {
            var redirected = CheckFilenameAndRedirect(filename, downloadurl, token);
            if (redirected.redirected)
            {
                return new RedirectResult(redirected.url, true);
            }
            return moodleFactory.DownloadFile(filename, downloadurl, token, Response);
        }

        // returns true if redirected
        private RedirectWrapper CheckFilenameAndRedirect(string filename, string downloadurl, string token)
        {
            // Downloadurl can have 4 or 5 parameters separated by /.
            // It's possible, that downloadurl also has some more parameters after a questionmark.
            // Example:
            // /234   /mod_resource/        1/anarcho-communist.jpg     ?forcedownload=1
            // /134000/mod_resource/content/1/Junge-Katze-Erziehen.jpg  ?forcedownload=1
            // We need to get the filename. That's the 4th or 5th / parameter (the last).
            // So we are splitting on / and taking the last part, that contains the filename
            // and possibly some more parameters. These parameters are cut of by cutting on
            // the questionmark and taking the first part.
            // split downloadurl to get filename parameter
            string[] downloadurl_splitted = downloadurl.Split(new char[] { '/' });
            var real_name_plus_params = downloadurl_splitted[downloadurl_splitted.Length - 1];
            string[] real_name_plus_params_splitted = real_name_plus_params.Split(new char[] { '?' });
            var real_name = real_name_plus_params_splitted[0];
            // compare filenames
            if (filename != real_name)
            {
                // create real url
                var redirect_url = real_name + "?downloadurl=" + downloadurl + "&token=" + token;
                // redirect
                return new RedirectWrapper(redirect_url);
            }
            return new RedirectWrapper();
        }

    }
}
