Dies ist der Moodle Microservice. Er greift auf das RWTH Moodle zu und bereitet die Daten für die App auf.

## Environment
Der Microservice wird durch folgenden Environment Variablen konfiguriert:

```bash
MOODLE_EXT_TOKEN_LOGIN_USER="rwthapp_proxy_user"
MOODLE_WEBSERVICE="rwthapp_proxy"
MOODLE_URL="https://d-mo01.devlef.campus.rwth-aachen.de/moodlems/"
MOODLE_ADMIN_TOKEN="GEHEIM"
```

Um die notwendige Konfiguration zu kommen, musst du dich an das IT Center wenden.

## API
- `/eLearning/Moodle/Test` - GET
    - Gibt die URL des Moodle Backends aus
